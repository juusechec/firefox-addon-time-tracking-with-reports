

## Publicar
- https://developer.mozilla.org/es/Add-ons/WebExtensions/Publishing_your_WebExtension

## Anatomía de una extension
- https://developer.mozilla.org/es/Add-ons/WebExtensions/Anatomia_de_una_WebExtension

## Tu segunda extensión
- https://developer.mozilla.org/es/Add-ons/WebExtensions/Tutorial

## API
- https://developer.mozilla.org/es/Add-ons/WebExtensions/API

## Iniciando con web-ext
- https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext

## Distribuir
- https://developer.mozilla.org/en-US/Add-ons/Distribution
- https://addons.mozilla.org/es/developers/addon/submit/agreement

## Ejemplos
- https://github.com/mdn/webextensions-examples
- https://github.com/iaaflaafc/Time-Track
- https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Working_with_files
- https://developer.mozilla.org/es/Add-ons/WebExtensions/Implement_a_settings_page
- https://github.com/mohsen1/json-formatter-js
