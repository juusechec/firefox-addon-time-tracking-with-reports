// GET URL browser.runtime.getURL('README.md');

const refreshTime = 60 * 1000; // 60 seconds refresh stop time
//const refreshTime = 2000; // only for quick tests!
const site = document.location.host;

let globalIndex = null;

function createUuid() {
  return (new Date()).getTime() + (Math.floor(Math.random() * 1000));
}

function getTitle() {
  return document.title;
}

function getUrl() {
  return document.location.href;
}

function createCurrentTime(){
  return (new Date()).getTime();
}

let initialWork = {
  'uuid': createUuid(),
  'title': getTitle(),
  'site': site,
  'url': getUrl(),
  'start': createCurrentTime(),
  'stop': null,
};

function resetInitialWork() {
  initialWork.uuid = createUuid();
  initialWork.title = getTitle();
  initialWork.url = getUrl(); // It's a good idea?
  initialWork.start = createCurrentTime();
  initialWork.stop = null;
  globalIndex = null; // reset index!
}

function saveWork(opt) {
  let history = browser.storage.local.get('work', (store) => {
    let work = store['work'];
    const today = new Date().toLocaleString().split(',')[0];
    if (work === undefined) {
      work = {};
      work[today] = [];
    }
    let workToday = work[today];
    const stopTime = (new Date()).getTime();
    initialWork.stop = stopTime;
    if (opt === undefined) { // add new
      console.log('Adding New...');
      globalIndex = workToday.push(initialWork);
    } else if(opt === 'update') {
      console.log('Updating...');
      //var globalIndex = workToday.findIndex(x => x.uuid === uuid);
      workToday[globalIndex] = initialWork;
    }
    browser.storage.local.set({
        'work': work
    });
    console.log('Saved work:', initialWork);
  });
}

function updateStop() {
  saveWork('update');
}

function areYouWorking() {
  const hasFocus = document.hasFocus();
  const isVisible = !Visibility.isHidden();
  const isConsoleOpen = devtools.open;
  return isVisible && (hasFocus || isConsoleOpen);
}

function verifyWorking() {
  // https://howchoo.com/g/mdg5otdhmzk/determine-if-a-tab-has-focus-in-javascript
  console.log('\nareYouWorking', areYouWorking(), 'index', globalIndex);
  if (areYouWorking()) {
    if (globalIndex === null) {
      // is not saved the first time
      saveWork();
    } else {
      // updated the first time work saved
      saveWork('update');
    }
  }
}

function printAll() {
  let history = browser.storage.local.get('work', (store) => {
    let work = store['work'];
    console.log('work', work);
  });
}

function cleanStore() {
  //browser.storage.local.clear();
  browser.storage.local.remove('work');
}

function setListenerVisibility() {
  document.addEventListener(Visibility.visibilitychange, function(e) {
    var hidden = Visibility.isHidden();
    if (hidden) {
      console.log('Change visibility and is hidden');
      updateStop();
      resetInitialWork();
    } else {
      console.log('Change visibility and is visible');
      resetInitialWork();
    }
  });
}

//cleanStore();
//printAll();
setInterval(verifyWorking, refreshTime); // each refreshTime seconds
setListenerVisibility();

console.log('Time Tracking with Reports was Loaded!');
