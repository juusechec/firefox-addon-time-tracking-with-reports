// ver http://www.chartjs.org/samples/latest/charts/bar/horizontal.html

var hColor = Chart.helpers.color;


function plotChar (horizontalBarChartData) {
  var ctx = document.getElementById('canvas').getContext('2d');
	window.myHorizontalBar = new Chart(ctx, {
		type: 'horizontalBar',
		data: horizontalBarChartData,
		options: {
			// Elements options apply to all of the options unless overridden in a dataset
			// In this case, we are setting the border of each horizontal bar to be 2px wide
			elements: {
				rectangle: {
					borderWidth: 2,
				}
			},
			responsive: true,
			legend: {
				position: 'right',
			},
			title: {
				display: true,
				text: 'Sitios más visitados!'
			}
		}
	});
}

function queryData (work) {
  const today = new Date().toLocaleString().split(',')[0];
  const workToday = work[today];
  console.log('workToday', workToday);
  let sites = {};
  for (let i = 0; i < workToday.length; i++) {
    const registry = workToday[i];
    const diffTime = registry.stop - registry.start;
    console.log('diffTime', registry.site, diffTime);
    if (sites[registry.site] === undefined) {
      sites[registry.site] = diffTime / 3600000; // time in hours
    } else {
      sites[registry.site] += diffTime / 3600000; // hours
    }
  }

  console.log('work sites', sites);

  let labels = [];
  let data = [];

  for (let site in sites) {
    let value = sites[site];
    value = Math.round(value * 100) / 100;
    labels.push(site);
    data.push(value);
  }

  // groupedProjects = dataProjects.reduce(function (r, a) {
  //     var key = a.attributes[criterioIdField];
  //     key = (key === null || key === 'null')?'-1':key+'';
  //     r[key] = r[key] || [];
  //     r[key].push(a);
  //     return r;
  //   }, {});

  var horizontalBarChartData = {
    labels: labels,
    datasets: [{
      label: 'Horas',
      backgroundColor: hColor(window.colors.red).alpha(0.5).rgbString(),
      borderColor: window.colors.red,
      borderWidth: 1,
      data: data
    }]
  };
  plotChar(horizontalBarChartData);
}

function loadData() {
  let history = browser.storage.local.get('work');
  history.then((store) => {
    let work = store['work'];
    if (work === undefined) {
      document.body.innerHTML += '<h1>No hay datos</h1>';
      return;
    }
    queryData(work);
  });
}

window.onload = function() {
  loadData();
};
