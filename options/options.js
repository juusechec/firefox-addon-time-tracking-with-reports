function saveOptions(e) {
  chrome.storage.local.set({
    'settings': {
      regex: document.querySelector('#regex').value,
      include_private: document.querySelector('#include_private').checked
    }
  });
}

function restoreOptions() {
  chrome.storage.local.get('settings', (res) => {
    console.log('settings', res);
    const settings = res['settings'] || {};
    document.querySelector('#regex').value = settings.regex || '*';
    document.querySelector('#include_private').checked = (settings.include_private !== undefined)?settings.include_private:true;
  });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector('form').addEventListener('submit', saveOptions);

function clearAll() {
  browser.storage.local.clear((res) => {
    window.location.reload();
  });
}

function clearHistory() {
  browser.storage.local.remove('work');
}

function clearSettings() {
  let rm = browser.storage.local.remove('settings', (res) => {
    window.location.reload();
  });
}

document.querySelector('#clearAll').addEventListener('click', clearAll);
document.querySelector('#clearHistory').addEventListener('click', clearHistory);
document.querySelector('#clearSettings').addEventListener('click', clearSettings);
